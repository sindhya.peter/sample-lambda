import { Duration, Stack, StackProps,  } from 'aws-cdk-lib';
import * as lambda from 'aws-cdk-lib/aws-lambda';

import { Construct } from 'constructs';
//  import * as sqs from 'aws-cdk-lib/aws-sqs';

export class LambdaStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const imgLayer = new lambda.LayerVersion(this, 'image-layer', {
      compatibleRuntimes: [
        lambda.Runtime.NODEJS_16_X,
        lambda.Runtime.NODEJS_14_X,
      ],
      code: lambda.Code.fromAsset('layers/nodejs.zip'),
      description: 'Image utilities',
    });
    new lambda.Function(this, 'LambdaNodeStack', {
      code: lambda.Code.fromAsset('./src'),
      functionName: "cdkLambdaNode",
      handler: 'index.handler',
      memorySize: 1024,
      runtime: lambda.Runtime.NODEJS_16_X,
      timeout: Duration.seconds(300),
      layers : [imgLayer],
    });
  }
}
